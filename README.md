# showme



## Description du projet

Projet M1 Environnement du libre du groupe A.
C'est un mod pour Minetest, qui a pour but de pouvoir afficher une image en jeu.

L'idée initiale est d'avoir un input où entrer l'URL d'une image lorsque l'on clique sur le cadre où sera visible l'image.

## Historique du projet

### 1. Découverte de Minetest (~2h)

La première étape a été simplement de tester Minetest voir les possibilités qu'offre le jeu ainsi que parcourir les différents mods disponible sur [ContentDB](https://content.minetest.net/) par la communauté.

### 2. Documentation (~4h)

La seconde phase a consisté à se documenter sur la création d'un mod pour Minetest. Différentes sources sont disponibles, dont ce [guide pour débuter](https://rubenwardy.com/minetest_modding_book/en/index.html) ainsi que cette [documentation détaillée](https://minetest.gitlab.io/minetest/).

N'ayant encore jamais programmé en Lua, cette [documentation](https://www.lua.org/pil/contents.html) m'a également été utile.

En me documentant je me suis prêté également à l'exercice en y faisant quelques tests pour mieux appréhender certaines fonctionnalités.

### 3. Prémices de Showme (~2h)

La première étape a été de créer mon bloc qu'on appelle "Node" ou noeud en français, l'idée a été d'avoir un cadre avec la photo à l'intérieur.

Après quelques recherches, je me suis aperçu qu'il fallait pour avoir un model aussi spécifique créer un modèle 3D en utilisant des outils tel que [Blockbench](https://www.blockbench.net/) et l'exporter en format ".obj".

Il faut également que je crée mes textures pour mon cadre, en comprenant le cadre ainsi que l'intérieur du cadre.

Et biensûr définir mon noeud avec tous ses paramètres qui vont bien dont le placement qui fait face au joueur quand il place le cadre.

Et voici le résultat en image :

![image du cadre](./screenshots/1.png)

### 4. Interface cliquable (~1h)

Ensuite il a fallut créer l'interface lorsque l'on clique sur le tableau afin d'y spécifier l'URL de l'image qu'on souhaite afficher.

Il faut également que l'URL reste stockée sur le cadre, j'ai également fait en sorte qu'après mise à jour de l'URL lorsque l'on réouvre l'interface l'URL soit déjà pré-remplis dans le champ prévu à cet effet.

Voici l'interface en image :

![image de l'interface](./screenshots/2.png)

### 5. Actualisé la texture du noeud (~7h)

La partie non-terminée à ce jour, pour cause différents problèmes me sont parvenus.

Le premier problème est celui qu'à également rencontré Clément, les requêtes http étant à nos risques et périls, par défaut Minetest désactive cette fonctionnalité.

Pour remédier à ça chaque joueur hébergeant leur monde devrait modifier les configurations de leur monde.

Donc, il m'est venu une solution secondaire, avoir la possibilité d'uploader les images manuellement et de les stocker sur le monde du joueur et le mod viendrait simplement récupérer l'image.

Suite à ça je me suis aperçu que la texture ne s'actualisait pas, j'ai débuggé jusqu'à réalisé que pour une raison que j'ignore encore à ce jour la variable qui stocke les textures quand je souhaite la récupérer me retourne un champ vide mais également après modification.

Me voilà dans une impasse que j'essaie de résoudre depuis plusieurs heures en vain.
Il va probablement falloir que je trouve une solution en sollicitant les forums, et en continuant d'effectuer d'autres tests et d'autres recherches..

**01/12/2023**
En approndissant mes recherches sur le problème, je me suis aperçu que c'est un vrai sujet de conversation et qu'[il n'existe pas vraiment de solution viable à ce jour](https://github.com/minetest/minetest/issues/3528).

[Une des meilleures solutions que j'ai pu trouver](https://github.com/minetest/minetest/issues/9193) est de définir au sein du noeud une liste (de textures) et d'avoir un paramètre qui sert de curseur sur la liste afin de sélectionner la texture qu'on souhaite.

Deux problèmes persistent, les textures doivent être préchargés au préalable, donc si on souhaite ajouter des textures il faut redémarrer notre monde.
Et l'autre problème, c'est que cette solution utilise un paramètre qu'on utilise pour mémoriser le positionnement de notre cadre (dans quelle direction il doit faire face en l'occurence).

### 6. Idées pour la suite

- [ ] Améliorer la sécurité relatif à la saisie
- [ ] Vérifier si le fichier spécifié existe
- [ ] Avoir différentes tailles de cadre
- [ ] Ajouter le mod sur [ContentDB](https://content.minetest.net/)