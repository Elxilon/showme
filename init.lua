local default_path = minetest.get_modpath("showme")

local modname = minetest.get_current_modname()

local texture = {"showme_rickroll.png^showme_frame.png"}

-- bibliothèque http pour télécharger l'image
local http = minetest.request_http_api()

minetest.register_on_player_receive_fields(function(player, formname, fields)

    if formname == modname .. ":frame_form" then
        local player_name = player:get_player_name()
 
        if fields.url_field and fields.change_texture then

            local pos = minetest.string_to_pos(player:get_meta():get_string("showme_pos"))
            local url = fields.url_field

            -- local img_folder = minetest.get_worldpath().."/mods/"..modname.."/"

            texture = {url.."^showme_frame.png"}

            -- Télécharge l'image depuis l'URL, cette partie nécessite le support HTTP par le serveur
            if http then
                http.fetch(url, {}, function(res)
                    if res.code == 200 then
                        -- Le téléchargement a réussi, enregistre l'image localement dans le monde
                        local file = io.open(minetest.get_worldpath().."/mods/"..modname.."/downloaded_image.png", "w")
                        texture = {"downloaded_image.png^showme_frame.png"}
                        file:write(res.data)
                        file:close()
                    else
                        minetest.chat_send_player(player_name, "Erreur de téléchargement de l'image : " .. res.code)
                    end
                end)
            end

            -- Garde en mémoire le lien sur le noeud
            local meta = minetest.get_meta(pos)
            meta:set_string("image_url", url)
            
            local node = minetest.get_node(pos)
            if node.tiles then
                minetest.chat_send_player(player_name, "Texture actuelle du noeud : " .. table.concat(node.tiles, " "))
            else
                minetest.chat_send_player(player_name, "Le noeud actuel ne spécifie pas de textures.")
            end

            -- Met à jour la texture du noeud
            minetest.swap_node(pos, {name = modname ..":frame", param2 = minetest.get_node(pos).param2, tiles = texture})
            
            node = minetest.get_node(pos)
            if node.tiles then
                minetest.chat_send_player(player_name, "Texture actuelle du noeud : " .. table.concat(node.tiles, " "))
            else
                minetest.chat_send_player(player_name, "Le noeud actuel ne spécifie pas de textures.")
            end

        end
    end
end)

minetest.register_node(modname .. ":frame", {
    description = "Dynamic frame",
    drawtype = "mesh",
    mesh = "showme_frame.obj",
    selection_box = {type = "fixed", fixed = {-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}},
    collision_box = {type = "fixed", fixed = {-0.5, -0.5, 0.4375, 0.5, 0.5, 0.5}},
    tiles = texture,
    paramtype = "light",
    paramtype2 = "facedir",
    sunlight_propagates = true,
    groups = {choppy = 3, oddly_breakable_by_hand = 3},

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        local player_name = clicker:get_player_name()

        -- Stocke la position dans les métadonnées du joueur
        local player_meta = minetest.get_player_by_name(player_name):get_meta()
        player_meta:set_string("showme_pos", minetest.pos_to_string(pos))
        
        local label = "Image URL:"
        if not http then
            label = "Image name + format (e.g. my_image.png):"
        end

        local val = minetest.get_meta(pos):get_string("image_url")

        minetest.show_formspec(player_name, modname .. ":frame_form",
           "size[8,1.5]" ..
           "field[0.5,0.5;7.5,1;url_field;" .. label .. ";" .. val .. "]" ..
           "button_exit[3,1;2,1;change_texture;Update]"
        )
    end,
})